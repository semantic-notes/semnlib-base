"use strict";
function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(require("@upe/typedjson"));
__export(require("./onto-service/user"));
__export(require("./onto-service/ontology"));
__export(require("./onto-service/thread"));
__export(require("./onto-service/notes"));
__export(require("./onto-service/IOnto"));
__export(require("./onto-service/onto-service.service"));
__export(require("./onto-service/utils"));
__export(require("./onto-service/ontology-utils"));
__export(require("./onto-service/semn-utils"));
//# sourceMappingURL=index.js.map