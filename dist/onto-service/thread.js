"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typedjson_1 = require("@upe/typedjson");
const lodash_1 = require("lodash");
const ts_option_1 = require("ts-option");
const ontology_1 = require("./ontology");
const utils_1 = require("./utils");
let CreateThread = class CreateThread {
    constructor(params) {
        lodash_1.merge(this, params);
    }
};
__decorate([
    typedjson_1.JsonMember({ type: String }),
    __metadata("design:type", String)
], CreateThread.prototype, "name", void 0);
__decorate([
    typedjson_1.JsonMember({ type: String }),
    __metadata("design:type", String)
], CreateThread.prototype, "description", void 0);
CreateThread = __decorate([
    typedjson_1.JsonObject(),
    __metadata("design:paramtypes", [Object])
], CreateThread);
exports.CreateThread = CreateThread;
let UserThread = class UserThread extends ontology_1.IOntology {
};
__decorate([
    typedjson_1.JsonMember({ type: String }),
    __metadata("design:type", String)
], UserThread.prototype, "uuid", void 0);
__decorate([
    typedjson_1.JsonMember({ type: String }),
    __metadata("design:type", String)
], UserThread.prototype, "name", void 0);
__decorate([
    typedjson_1.JsonMember({ type: String }),
    __metadata("design:type", String)
], UserThread.prototype, "description", void 0);
__decorate([
    typedjson_1.JsonMember({
        toJsonMap: utils_1.genOptionToRaw(),
        toObjectMap: utils_1.genRawToOptionMap(ontology_1.OntoEntityDto),
    }),
    __metadata("design:type", ts_option_1.Option)
], UserThread.prototype, "annotations", void 0);
__decorate([
    typedjson_1.JsonMember({
        toJsonMap: utils_1.genOptionToRaw(),
        toObjectMap: utils_1.genRawToOptionMap(ontology_1.OntoClassDto),
    }),
    __metadata("design:type", ts_option_1.Option)
], UserThread.prototype, "classes", void 0);
__decorate([
    typedjson_1.JsonMember({
        toJsonMap: utils_1.genOptionToRaw(),
        toObjectMap: utils_1.genRawToOptionMap(ontology_1.OntoRelationDto),
    }),
    __metadata("design:type", ts_option_1.Option)
], UserThread.prototype, "relations", void 0);
__decorate([
    typedjson_1.JsonMember({
        toJsonMap: utils_1.genOptionToRaw(),
        toObjectMap: utils_1.genRawToOptionMap(ontology_1.OntoDataPropDto),
    }),
    __metadata("design:type", ts_option_1.Option)
], UserThread.prototype, "dataProperties", void 0);
__decorate([
    typedjson_1.JsonMember({
        toJsonMap: utils_1.genOptionToRaw(),
        toObjectMap: utils_1.genRawToOptionMap(ontology_1.OntoIndividualDto, true),
    }),
    __metadata("design:type", ts_option_1.Option)
], UserThread.prototype, "individuals", void 0);
UserThread = __decorate([
    typedjson_1.JsonObject()
], UserThread);
exports.UserThread = UserThread;
//# sourceMappingURL=thread.js.map