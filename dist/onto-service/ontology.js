"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var OntoClassDto_1, OntoRelationDto_1, OntoDataPropDto_1, OntoIndividualDto_1;
"use strict";
const typedjson_1 = require("@upe/typedjson");
const lodash_1 = require("lodash");
const ts_option_1 = require("ts-option");
const utils_1 = require("./utils");
var OwlIRIs;
(function (OwlIRIs) {
    OwlIRIs["ontoClass"] = "rdf:type";
})(OwlIRIs = exports.OwlIRIs || (exports.OwlIRIs = {}));
var OwlAnnotations;
(function (OwlAnnotations) {
    OwlAnnotations["label"] = "rdfs:label";
    OwlAnnotations["comment"] = "rdfs:comment";
})(OwlAnnotations = exports.OwlAnnotations || (exports.OwlAnnotations = {}));
var DataIRIs;
(function (DataIRIs) {
    DataIRIs["hasConceptDescr"] = "baseOntology:hasItemDescription";
    DataIRIs["hasText"] = "baseOntology:hasText";
    DataIRIs["hasTitle"] = "baseOntology:hasTitle";
})(DataIRIs = exports.DataIRIs || (exports.DataIRIs = {}));
var SystemClass;
(function (SystemClass) {
    SystemClass["InfoItem"] = "baseOntology:InfoItem";
    SystemClass["System"] = "baseOntology:System";
    SystemClass["Unhandled"] = "baseOntology:Unhandled";
})(SystemClass = exports.SystemClass || (exports.SystemClass = {}));
var ConceptClass;
(function (ConceptClass) {
    ConceptClass["Concept"] = "baseOntology:Concept";
})(ConceptClass = exports.ConceptClass || (exports.ConceptClass = {}));
var SystemRelations;
(function (SystemRelations) {
    SystemRelations["mayBeIsRelated"] = "baseOntology:mayBeIsRelated";
    SystemRelations["wasUpdated"] = "baseOntology:wasUpdated";
    SystemRelations["wasCreated"] = "baseOntology:wasCreated";
})(SystemRelations = exports.SystemRelations || (exports.SystemRelations = {}));
exports.toFilterSystemRelations = [
    SystemRelations.wasCreated, SystemRelations.wasUpdated,
];
exports.systemClasses = [
    SystemClass.InfoItem, SystemClass.System, SystemClass.Unhandled,
];
exports.toFilterSystemClasses = [
    SystemClass.System,
];
function getShortName(iriVal) {
    return lodash_1.chain(iriVal.split(/(#|:)/))
        .last()
        .value();
    // if (iriVal.includes('#')) {
    //   return iriVal.split('#')[1];
    // } else {
    //   return iriVal.split(':')[1];
    // }
}
exports.getShortName = getShortName;
let OntoIRI = class OntoIRI {
    constructor(iri) {
        this.iri = '';
        if (iri) {
            this.iri = iri;
        }
    }
    getLocalId() {
        return getShortName(this.iri);
    }
};
__decorate([
    typedjson_1.JsonMember({ type: String }),
    __metadata("design:type", String)
], OntoIRI.prototype, "iri", void 0);
OntoIRI = __decorate([
    typedjson_1.JsonObject(),
    __metadata("design:paramtypes", [String])
], OntoIRI);
exports.OntoIRI = OntoIRI;
let OntoValue = class OntoValue extends OntoIRI {
    constructor(params) {
        super();
        this.inferred = false;
        if (!lodash_1.isNil(params)) {
            lodash_1.merge(this, params);
        }
    }
    getLocalId() {
        return getShortName(this.value);
    }
};
__decorate([
    typedjson_1.JsonMember({ type: String }),
    __metadata("design:type", String)
], OntoValue.prototype, "value", void 0);
__decorate([
    typedjson_1.JsonMember({ type: Boolean }),
    __metadata("design:type", Boolean)
], OntoValue.prototype, "inferred", void 0);
OntoValue = __decorate([
    typedjson_1.JsonObject(),
    __metadata("design:paramtypes", [Object])
], OntoValue);
exports.OntoValue = OntoValue;
let OntoEntityDto = class OntoEntityDto extends OntoIRI {
    constructor(iri) {
        super(iri);
        this.annotations = [];
    }
    getAnnotationsValues(iri) {
        return this.annotations.filter(el => el.iri === iri);
    }
};
__decorate([
    typedjson_1.JsonMember({ elements: OntoValue }),
    __metadata("design:type", Array)
], OntoEntityDto.prototype, "annotations", void 0);
OntoEntityDto = __decorate([
    typedjson_1.JsonObject(),
    __metadata("design:paramtypes", [String])
], OntoEntityDto);
exports.OntoEntityDto = OntoEntityDto;
let TreeOntoItem = class TreeOntoItem extends OntoEntityDto {
    constructor(iri) {
        super(iri);
        this.subItems = [];
    }
};
TreeOntoItem = __decorate([
    typedjson_1.JsonObject(),
    __metadata("design:paramtypes", [String])
], TreeOntoItem);
exports.TreeOntoItem = TreeOntoItem;
let OntoClassDto = OntoClassDto_1 = class OntoClassDto extends TreeOntoItem {
};
__decorate([
    typedjson_1.JsonMember({ elements: OntoClassDto_1 }),
    __metadata("design:type", Array)
], OntoClassDto.prototype, "subItems", void 0);
OntoClassDto = OntoClassDto_1 = __decorate([
    typedjson_1.JsonObject()
], OntoClassDto);
exports.OntoClassDto = OntoClassDto;
let OntoRelationDto = OntoRelationDto_1 = class OntoRelationDto extends TreeOntoItem {
};
__decorate([
    typedjson_1.JsonMember({ elements: OntoRelationDto_1 }),
    __metadata("design:type", Array)
], OntoRelationDto.prototype, "subItems", void 0);
OntoRelationDto = OntoRelationDto_1 = __decorate([
    typedjson_1.JsonObject()
], OntoRelationDto);
exports.OntoRelationDto = OntoRelationDto;
let OntoDataPropDto = OntoDataPropDto_1 = class OntoDataPropDto extends TreeOntoItem {
};
__decorate([
    typedjson_1.JsonMember({ elements: OntoDataPropDto_1 }),
    __metadata("design:type", Array)
], OntoDataPropDto.prototype, "subItems", void 0);
OntoDataPropDto = OntoDataPropDto_1 = __decorate([
    typedjson_1.JsonObject()
], OntoDataPropDto);
exports.OntoDataPropDto = OntoDataPropDto;
let OntoIndividualDto = OntoIndividualDto_1 = class OntoIndividualDto extends OntoEntityDto {
    constructor() { super(); }
    static create(params = {}) {
        const ret = new OntoIndividualDto_1();
        lodash_1.defaults(params, {
            annotations: [],
            blankNodes: [],
            classes: [],
            dataProperties: [],
            relations: [],
        });
        lodash_1.merge(ret, params);
        return ret;
    }
    hasClassIri(cls) {
        return !lodash_1.isEmpty(this.classes) && this.classes.some(el => el.value === cls);
    }
    getNameClasses(withSystem = false) {
        return this.classes
            .filter(el => withSystem
            ? true
            : !exports.toFilterSystemClasses.some(cls => cls === el.value))
            .map(el => el.value.split('#')[1]);
    }
    getRelationValues(iri) {
        return this.relations.filter(el => el.iri === iri);
    }
    getDataValues(iri) {
        return this.dataProperties.filter(el => el.iri === iri);
    }
    getData(iri) {
        return this.getDataValues(iri).map(el => el.value);
    }
};
__decorate([
    typedjson_1.JsonMember({ elements: OntoValue }),
    __metadata("design:type", Array)
], OntoIndividualDto.prototype, "annotations", void 0);
__decorate([
    typedjson_1.JsonMember({ elements: OntoValue }),
    __metadata("design:type", Array)
], OntoIndividualDto.prototype, "classes", void 0);
__decorate([
    typedjson_1.JsonMember({ elements: OntoValue }),
    __metadata("design:type", Array)
], OntoIndividualDto.prototype, "relations", void 0);
__decorate([
    typedjson_1.JsonMember({ elements: OntoValue }),
    __metadata("design:type", Array)
], OntoIndividualDto.prototype, "dataProperties", void 0);
__decorate([
    typedjson_1.JsonMember({ elements: OntoIndividualDto_1 }),
    __metadata("design:type", Array)
], OntoIndividualDto.prototype, "blankNodes", void 0);
OntoIndividualDto = OntoIndividualDto_1 = __decorate([
    typedjson_1.JsonObject(),
    __metadata("design:paramtypes", [])
], OntoIndividualDto);
exports.OntoIndividualDto = OntoIndividualDto;
let IOntology = class IOntology {
};
__decorate([
    typedjson_1.JsonMember({
        toJsonMap: utils_1.genOptionToRaw(),
        toObjectMap: utils_1.genRawToOptionMap(OntoEntityDto),
    }),
    __metadata("design:type", ts_option_1.Option)
], IOntology.prototype, "annotations", void 0);
__decorate([
    typedjson_1.JsonMember({
        toJsonMap: utils_1.genOptionToRaw(),
        toObjectMap: utils_1.genRawToOptionMap(OntoClassDto),
    }),
    __metadata("design:type", ts_option_1.Option)
], IOntology.prototype, "classes", void 0);
__decorate([
    typedjson_1.JsonMember({
        toJsonMap: utils_1.genOptionToRaw(),
        toObjectMap: utils_1.genRawToOptionMap(OntoRelationDto),
    }),
    __metadata("design:type", ts_option_1.Option)
], IOntology.prototype, "relations", void 0);
__decorate([
    typedjson_1.JsonMember({
        toJsonMap: utils_1.genOptionToRaw(),
        toObjectMap: utils_1.genRawToOptionMap(OntoDataPropDto),
    }),
    __metadata("design:type", ts_option_1.Option)
], IOntology.prototype, "dataProperties", void 0);
__decorate([
    typedjson_1.JsonMember({
        toJsonMap: utils_1.genOptionToRaw(),
        toObjectMap: utils_1.genRawToOptionMap(OntoIndividualDto, true),
    }),
    __metadata("design:type", ts_option_1.Option)
], IOntology.prototype, "individuals", void 0);
IOntology = __decorate([
    typedjson_1.JsonObject()
], IOntology);
exports.IOntology = IOntology;
//# sourceMappingURL=ontology.js.map