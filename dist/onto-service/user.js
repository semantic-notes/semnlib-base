"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var User_1;
"use strict";
const typedjson_1 = require("@upe/typedjson");
const lodash_1 = require("lodash");
let User = User_1 = class User {
    static fromRaw(params) {
        return lodash_1.merge(new User_1(), params);
    }
};
__decorate([
    typedjson_1.JsonMember({ type: String }),
    __metadata("design:type", String)
], User.prototype, "id", void 0);
__decorate([
    typedjson_1.JsonMember({ type: String }),
    __metadata("design:type", String)
], User.prototype, "username", void 0);
__decorate([
    typedjson_1.JsonMember({ type: String }),
    __metadata("design:type", String)
], User.prototype, "email", void 0);
User = User_1 = __decorate([
    typedjson_1.JsonObject()
], User);
exports.User = User;
let UserAuth = class UserAuth {
};
__decorate([
    typedjson_1.JsonMember({ type: String }),
    __metadata("design:type", String)
], UserAuth.prototype, "id", void 0);
__decorate([
    typedjson_1.JsonMember({ type: Number }),
    __metadata("design:type", Number)
], UserAuth.prototype, "tokenExpTime", void 0);
__decorate([
    typedjson_1.JsonMember({ type: String }),
    __metadata("design:type", String)
], UserAuth.prototype, "token", void 0);
__decorate([
    typedjson_1.JsonMember({ type: String }),
    __metadata("design:type", String)
], UserAuth.prototype, "refresh", void 0);
UserAuth = __decorate([
    typedjson_1.JsonObject()
], UserAuth);
exports.UserAuth = UserAuth;
let UserLoginToken = class UserLoginToken {
};
__decorate([
    typedjson_1.JsonMember({ type: String }),
    __metadata("design:type", String)
], UserLoginToken.prototype, "token", void 0);
UserLoginToken = __decorate([
    typedjson_1.JsonObject()
], UserLoginToken);
exports.UserLoginToken = UserLoginToken;
//# sourceMappingURL=user.js.map