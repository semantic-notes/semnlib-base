import { ElementType, ISerializerSettings } from '@upe/typedjson';
import { Option } from 'ts-option';
export declare function genRawToOptionMap<T>(type: ElementType<T>, alwaysAsArray?: boolean, settings?: ISerializerSettings): (raw: any) => Option<T>;
export declare function genOptionToRaw<T>(settings?: ISerializerSettings): (raw: any) => any;
