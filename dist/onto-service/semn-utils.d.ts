import { IRI, OntoIndividualDto, OntoValue } from './ontology';
export declare class SemnUtils {
    static genOntoValue(params: {
        iri: IRI;
        value: string;
    }): OntoValue;
    static genNewConcept(params: {
        label: string;
    }): OntoIndividualDto;
    static genInfoItem(params: {
        title: string;
        text: string;
    }): OntoIndividualDto;
}
