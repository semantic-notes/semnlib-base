import { Option } from 'ts-option';
import { UserThread } from './thread';
export declare class User {
    static fromRaw(params: {
        id: string;
        username: string;
        email?: string;
    }): User;
    id: string;
    username: string;
    email: string;
}
export declare class UserAuth {
    id: string;
    tokenExpTime: number;
    token: string;
    refresh: string;
}
export declare class UserLoginToken {
    token: string;
}
export interface UserData {
    token: UserAuth;
    user: User;
    selectedThread: Option<UserThread>;
}
