"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function getEntityPath(root, iri) {
    const paths = [];
    const doEntityStep = (entity) => {
        paths.push(entity);
        if (entity.iri === iri) {
            return true;
        }
        else {
            if (entity.subItems.some(doEntityStep)) {
                return true;
            }
            else {
                paths.pop();
                return false;
            }
        }
    };
    doEntityStep(root);
    return paths;
}
exports.getEntityPath = getEntityPath;
function compareOntoPath(lha, rha) {
    const lhaHash = lha.map(el => el.getLocalId()).join('');
    const rhaHash = rha.map(el => el.getLocalId()).join('');
    return ((lhaHash === rhaHash) ? 0 : ((lhaHash > rhaHash) ? 1 : -1));
}
exports.compareOntoPath = compareOntoPath;
//# sourceMappingURL=ontology-utils.js.map