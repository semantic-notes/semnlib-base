"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const typedjson_1 = require("@upe/typedjson");
const axios_1 = require("axios");
const lodash_1 = require("lodash");
const ts_option_1 = require("ts-option");
const url_1 = require("url");
const ontology_1 = require("./ontology");
const thread_1 = require("./thread");
const user_1 = require("./user");
const rxjs_1 = require("rxjs");
let isNode = false;
let Agent;
let otplib;
try {
    isNode = Object.prototype.toString.call(global.process) === '[object process]';
    if (isNode) {
        // tslint:disable-next-line:no-var-requires
        const https = require('https');
        Agent = https.Agent;
        // tslint:disable-next-line:no-var-requires
        otplib = require('otplib');
    }
}
catch (e) { }
class OntoService {
    constructor(baseUri = 'http://localhost:9000/api', base32SecretBase = '') {
        this.baseUri = baseUri;
        this.base32SecretBase = base32SecretBase;
        // tslint:disable-next-line:member-access
        // tslint:disable-next-line:member-ordering
        this.errors = new rxjs_1.Subject();
        if (isNode) {
            const step = 30;
            otplib.authenticator.options = {
                step: step,
                window: 3,
            };
            this.url = new url_1.URL(this.baseUri);
            if (this.url.protocol === 'https:') {
                this.net = axios_1.default.create({
                    httpsAgent: new Agent({
                        rejectUnauthorized: false,
                    }),
                });
            }
            else {
                this.net = axios_1.default.create();
            }
        }
        else {
            this.net = axios_1.default.create();
        }
    }
    handleError(error) {
        this.errors.next(error);
    }
    deserialize(json, type, settings = {}) {
        return typedjson_1.tDeserialize(json, type, settings);
    }
    serialize(obj, settings = {}) {
        return typedjson_1.tSerialize(obj, settings);
    }
    loginUserByToken(token) {
        const url = `${this.baseUri}/login-by-token`;
        return this.net.post(url, {}, { headers: { Authorization: `Bearer ${token}` } })
            .then(response => ts_option_1.option(this.deserialize(response.data, user_1.UserAuth)), error => {
            this.handleError(error);
            throw error;
        });
    }
    loginWithGoogle(params) {
        const url = `${this.baseUri}/login-with-google?code=${params.code}&scope=${params.scope}`;
        return this.net.get(url, { headers: { 'callback-url': params.callBackUrl } })
            .then(response => ts_option_1.option(this.deserialize(response.data, user_1.UserAuth)), error => {
            this.handleError(error);
            throw error;
        });
    }
    loginUserFromTelegram(user) {
        const token = otplib.authenticator.generate(this.base32SecretBase);
        const userDto = this.serialize(user);
        const url = `${this.baseUri}/login-for-telegram`;
        return this.net.post(url, userDto, { headers: { totp: token } })
            .then(response => ts_option_1.option(this.deserialize(response.data, user_1.UserAuth)), error => {
            this.handleError(error);
            throw error;
        });
    }
    refreshToken(auth) {
        const url = `${this.baseUri}/refresh`;
        return this.net.post(url, {}, {
            headers: { Authorization: auth.refresh }
        })
            .then(response => ts_option_1.option(this.deserialize(response.data, user_1.UserAuth)))
            .catch(error => {
            this.handleError(error);
            return ts_option_1.none;
        });
    }
    genLoginToken(user, auth) {
        const from = 'telegram';
        const token = otplib.authenticator.generate(this.base32SecretBase);
        const url = `${this.baseUri}/get-login-token/${auth.id}?from=${from}`;
        const userDto = this.serialize(user);
        return this.net.post(url, userDto, {
            headers: {
                Authorization: auth.token,
                totp: token,
            },
        }).then(response => ts_option_1.option(this.deserialize(response.data, user_1.UserLoginToken)))
            .catch(error => {
            this.handleError(error);
            return ts_option_1.none;
        });
    }
    getUserData(auth) {
        const url = `${this.baseUri}/users/${auth.id}/data`;
        return this.net.get(url, this.getNetParams(auth))
            .then(resp => ts_option_1.option(this.deserialize(resp.data, user_1.User)))
            .catch((error) => {
            this.handleError(error);
            return ts_option_1.none;
        });
    }
    getThread(auth, uuid, detailed = false) {
        const url = `${this.baseUri}/users/${auth.id}/threads/${uuid}?detailed=${detailed}`;
        return this.net.get(url, this.getNetParams(auth))
            .then(resp => ts_option_1.option(this.deserialize(resp.data, thread_1.UserThread)))
            .catch((error) => {
            this.handleError(error);
            return ts_option_1.none;
        });
    }
    createThread(auth, thread) {
        const url = `${this.baseUri}/users/${auth.id}/threads`;
        const dto = this.serialize(thread);
        return this.net.post(url, dto, this.getNetParams(auth))
            .then((res) => `${res.data}`);
    }
    getThreads(auth) {
        const url = `${this.baseUri}/users/${auth.id}/threads`;
        return this.net.get(url, this.getNetParams(auth)).then(resp => {
            const items = resp.data;
            const dtos = items.map(el => this.deserialize(el, thread_1.UserThread));
            return lodash_1.isEmpty(dtos) ? ts_option_1.none : ts_option_1.some(dtos);
        })
            .catch(error => {
            this.handleError(error);
            return ts_option_1.none;
        });
    }
    addNoteToThread(auth, thread, note) {
        const url = `${this.baseUri}/users/${auth.id}/threads/${thread.uuid}/notes`;
        const dto = this.serialize(note);
        return this.net.post(url, dto, this.getNetParams(auth))
            .then(res => {
            return Promise.resolve(res);
        })
            .catch(error => {
            this.handleError(error);
        });
    }
    getIndividualsInThread(auth, thread) {
        const url = `${this.baseUri}/users/${auth.id}/threads/${thread.uuid}/ind`;
        return this.net.get(url, this.getNetParams(auth)).then(res => {
            const items = res.data;
            const dtos = items.map(el => this.deserialize(el, ontology_1.OntoIndividualDto));
            return lodash_1.isEmpty(dtos) ? ts_option_1.none : ts_option_1.some(dtos);
        }).catch(error => {
            this.handleError(error);
            return ts_option_1.none;
        });
    }
    getNotesInThread(auth, thread) {
        const url = `${this.baseUri}/users/${auth.id}/threads/${thread.uuid}/notes`;
        return this.net.get(url, this.getNetParams(auth)).then(res => {
            const items = res.data;
            const dtos = items.map(el => this.deserialize(el, ontology_1.OntoIndividualDto));
            return lodash_1.isEmpty(dtos) ? ts_option_1.none : ts_option_1.some(dtos);
        }).catch(error => {
            this.handleError(error);
            return ts_option_1.none;
        });
    }
    getEntity(auth, thread, uuid) {
        const url = `${this.baseUri}/users/${auth.id}/threads/${thread.uuid}/ind/${uuid}`;
        return this.net.get(url, this.getNetParams(auth)).then(res => {
            return ts_option_1.some(this.deserialize(res.data, ontology_1.OntoIndividualDto));
        }).catch(error => {
            this.handleError(error);
            return ts_option_1.none;
        });
    }
    createEntity(auth, thread, entity) {
        const url = `${this.baseUri}/users/${auth.id}/threads/${thread.uuid}/ind`;
        return this.net.post(url, this.serialize(entity), this.getNetParams(auth)).then(res => {
            return ts_option_1.some(this.deserialize(res.data, ontology_1.OntoIndividualDto));
        }).catch(error => {
            this.handleError(error);
            return ts_option_1.none;
        });
    }
    updateEntity(auth, thread, entity) {
        const url = `${this.baseUri}/users/${auth.id}/threads/${thread.uuid}/ind/${entity.getLocalId()}`;
        return this.net.put(url, this.serialize(entity), this.getNetParams(auth)).then(res => {
            return ts_option_1.some(this.deserialize(res.data, ontology_1.OntoIndividualDto));
        }).catch(error => {
            this.handleError(error);
            return ts_option_1.none;
        });
    }
    clearThread(auth, thread) {
        const url = `${this.baseUri}/users/${auth.id}/threads/${thread.uuid}/clear`;
        return this.net.put(url, {}, this.getNetParams(auth))
            .then(res => Promise.resolve());
    }
    getNetParams(data) {
        return {
            headers: {
                Authorization: data.token,
            },
        };
    }
}
exports.OntoService = OntoService;
//# sourceMappingURL=onto-service.service.js.map