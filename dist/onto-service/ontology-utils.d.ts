import { TreeOntoItem } from './ontology';
export declare type OntoEntityPath = TreeOntoItem[];
export declare function getEntityPath(root: TreeOntoItem, iri: string): TreeOntoItem[];
export declare function compareOntoPath(lha: OntoEntityPath, rha: OntoEntityPath): number;
