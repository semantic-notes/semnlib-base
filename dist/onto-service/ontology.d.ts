import { Option } from 'ts-option';
export declare type IRI = string;
export declare enum OwlIRIs {
    ontoClass = "rdf:type"
}
export declare enum OwlAnnotations {
    label = "rdfs:label",
    comment = "rdfs:comment"
}
export declare enum DataIRIs {
    hasConceptDescr = "baseOntology:hasItemDescription",
    hasText = "baseOntology:hasText",
    hasTitle = "baseOntology:hasTitle"
}
export declare enum SystemClass {
    InfoItem = "baseOntology:InfoItem",
    System = "baseOntology:System",
    Unhandled = "baseOntology:Unhandled"
}
export declare enum ConceptClass {
    Concept = "baseOntology:Concept"
}
export declare enum SystemRelations {
    mayBeIsRelated = "baseOntology:mayBeIsRelated",
    wasUpdated = "baseOntology:wasUpdated",
    wasCreated = "baseOntology:wasCreated"
}
export declare const toFilterSystemRelations: string[];
export declare const systemClasses: SystemClass[];
export declare const toFilterSystemClasses: SystemClass[];
export declare function getShortName(iriVal: string): string;
export declare class OntoIRI {
    iri: IRI;
    constructor(iri?: IRI);
    getLocalId(): string;
}
export declare class OntoValue extends OntoIRI {
    value: string;
    inferred: boolean;
    constructor(params?: {
        iri: IRI;
        value: string;
    });
    getLocalId(): string;
}
export declare class OntoEntityDto extends OntoIRI {
    annotations: OntoValue[];
    constructor(iri?: IRI);
    getAnnotationsValues(iri: OwlAnnotations | IRI): OntoValue[];
}
export declare class TreeOntoItem extends OntoEntityDto {
    subItems: TreeOntoItem[];
    constructor(iri?: IRI);
}
export declare class OntoClassDto extends TreeOntoItem {
    subItems: OntoClassDto[];
}
export declare class OntoRelationDto extends TreeOntoItem {
    subItems: OntoRelationDto[];
}
export declare class OntoDataPropDto extends TreeOntoItem {
    subItems: OntoDataPropDto[];
}
export declare class OntoIndividualDto extends OntoEntityDto {
    static create(params?: {
        classes?: OntoValue[];
        annotations?: OntoValue[];
        relations?: OntoValue[];
        dataProperties?: OntoValue[];
        blankNodes?: OntoIndividualDto[];
    }): OntoIndividualDto;
    annotations: OntoValue[];
    classes: OntoValue[];
    relations: OntoValue[];
    dataProperties: OntoValue[];
    blankNodes: OntoValue[];
    constructor();
    hasClassIri(cls: string): boolean;
    getNameClasses(withSystem?: boolean): string[];
    getRelationValues(iri: IRI): OntoValue[];
    getDataValues(iri: DataIRIs | IRI): OntoValue[];
    getData(iri: DataIRIs | IRI): string[];
}
export declare class IOntology {
    annotations: Option<OntoEntityDto>;
    classes: Option<OntoClassDto | OntoClassDto[]>;
    relations: Option<OntoRelationDto | OntoRelationDto[]>;
    dataProperties: Option<OntoDataPropDto | OntoDataPropDto[]>;
    individuals: Option<OntoIndividualDto[]>;
}
