export declare class CreateNoteDto {
    constructor(params: {
        text: string;
        title?: string;
    });
    title: string;
    text: string;
}
