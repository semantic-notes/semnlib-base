import { ElementType, ISerializerSettings } from '@upe/typedjson';
import { Option } from 'ts-option';
import { CreateNoteDto } from './notes';
import { OntoIndividualDto, IRI } from './ontology';
import { CreateThread, UserThread } from './thread';
import { User, UserAuth, UserLoginToken } from './user';
export declare abstract class IOnto {
    abstract deserialize<T>(json: Object, type: ElementType<T>, settings?: ISerializerSettings): T;
    abstract serialize<T>(obj: T, settings?: ISerializerSettings): T;
    abstract loginUserByToken(token: string): Promise<Option<UserAuth>>;
    abstract loginUserFromTelegram(user: User): Promise<Option<UserAuth>>;
    abstract loginWithGoogle(params: {
        code: string;
        scope: string;
        callBackUrl: string;
    }): Promise<Option<UserAuth>>;
    abstract refreshToken(auth: UserAuth): Promise<Option<UserAuth>>;
    abstract genLoginToken(user: User, auth: UserAuth): Promise<Option<UserLoginToken>>;
    abstract getUserData(auth: UserAuth): Promise<Option<User>>;
    abstract clearThread(auth: UserAuth, thread: UserThread): Promise<void>;
    abstract createThread(auth: UserAuth, thread: CreateThread, detailed: boolean): Promise<string>;
    abstract getThreads(auth: UserAuth): Promise<Option<UserThread[]>>;
    abstract getThread(auth: UserAuth, uuid: string): Promise<Option<UserThread>>;
    abstract addNoteToThread(auth: UserAuth, thread: UserThread, note: CreateNoteDto): Promise<void | any>;
    abstract getNotesInThread(auth: UserAuth, thread: UserThread): Promise<Option<OntoIndividualDto[]>>;
    abstract getIndividualsInThread(auth: UserAuth, thread: UserThread): Promise<Option<OntoIndividualDto[]>>;
    abstract getEntity(auth: UserAuth, thread: UserThread, uuid: IRI): Promise<Option<OntoIndividualDto>>;
    abstract createEntity(auth: UserAuth, thread: UserThread, entity: OntoIndividualDto): Promise<Option<OntoIndividualDto>>;
    abstract updateEntity(auth: UserAuth, thread: UserThread, entity: OntoIndividualDto): Promise<Option<OntoIndividualDto>>;
}
