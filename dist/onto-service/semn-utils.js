"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const ontology_1 = require("./ontology");
class SemnUtils {
    static genOntoValue(params) {
        return new ontology_1.OntoValue(params);
    }
    static genNewConcept(params) {
        const value = ontology_1.OntoIndividualDto.create({
            annotations: [
                SemnUtils.genOntoValue({ iri: ontology_1.OwlAnnotations.label, value: params.label }),
            ],
            classes: [
                SemnUtils.genOntoValue({ iri: ontology_1.OwlIRIs.ontoClass, value: ontology_1.ConceptClass.Concept }),
            ],
        });
        return value;
    }
    static genInfoItem(params) {
        const value = ontology_1.OntoIndividualDto.create({
            classes: [
                SemnUtils.genOntoValue({ iri: ontology_1.OwlIRIs.ontoClass, value: ontology_1.SystemClass.InfoItem }),
            ],
            dataProperties: [
                SemnUtils.genOntoValue({ iri: ontology_1.DataIRIs.hasTitle, value: params.title }),
                SemnUtils.genOntoValue({ iri: ontology_1.DataIRIs.hasText, value: params.text }),
            ],
        });
        return value;
    }
}
exports.SemnUtils = SemnUtils;
//# sourceMappingURL=semn-utils.js.map