import { Option } from 'ts-option';
import { IOntology, OntoClassDto, OntoDataPropDto, OntoEntityDto, OntoIndividualDto, OntoRelationDto } from './ontology';
export declare class CreateThread {
    constructor(params: {
        name: string;
        description: string;
    });
    name: string;
    description: string;
}
export declare class UserThread extends IOntology {
    uuid: string;
    name: string;
    description: string;
    annotations: Option<OntoEntityDto>;
    classes: Option<OntoClassDto | OntoClassDto[]>;
    relations: Option<OntoRelationDto | OntoRelationDto[]>;
    dataProperties: Option<OntoDataPropDto | OntoDataPropDto[]>;
    individuals: Option<OntoIndividualDto[]>;
}
