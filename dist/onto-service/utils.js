"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const typedjson_1 = require("@upe/typedjson");
const lodash_1 = require("lodash");
const ts_option_1 = require("ts-option");
const util_1 = require("util");
function genRawToOptionMap(type, alwaysAsArray = false, settings = {}) {
    return (raw) => {
        if (!lodash_1.isNil(raw)) {
            let value;
            if (util_1.isArray(raw)) {
                value = raw.map(el => typedjson_1.tDeserialize(el, type, settings));
            }
            else {
                const val = typedjson_1.tDeserialize(raw, type, settings);
                value = alwaysAsArray ? [val] : val;
            }
            return ts_option_1.option(value);
        }
        else {
            return ts_option_1.none;
        }
    };
}
exports.genRawToOptionMap = genRawToOptionMap;
function genOptionToRaw(settings = {}) {
    return (obj) => {
        return obj.map(value => {
            if (util_1.isArray(value)) {
                return value.map(el => typedjson_1.tSerialize(el, settings));
            }
            else {
                return typedjson_1.tSerialize(value, settings);
            }
        }).orNull;
    };
}
exports.genOptionToRaw = genOptionToRaw;
//# sourceMappingURL=utils.js.map