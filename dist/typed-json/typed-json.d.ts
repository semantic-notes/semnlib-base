import { Deserializer, ElementType, IJsonMemberOptions, ISerializerSettings, Serializer } from '@upe/typedjson';
export declare abstract class ITwJsonObject {
}
export declare class Any {
}
export declare class TwJsObject {
}
export declare function getConfigSettings(): ISerializerSettings;
export declare function twDeserialize<T>(json: Object, type: ElementType<T>, settings?: ISerializerSettings): T;
export declare function twSerialize<T>(obj: T, settings?: ISerializerSettings): T;
export declare class TwDeserializer<T extends ITwJsonObject> extends Deserializer<T> {
    constructor(json: any, type: ElementType<T>, settings?: ISerializerSettings, depth?: number);
    convertDate(value: string, pk: string): Date;
    processProperty(value: any, propertyKey: string, options: IJsonMemberOptions): any;
    transform(value: any, propertyKey: string, elementType: ElementType | undefined): any;
}
export declare class TwSerializer<T extends ITwJsonObject> extends Serializer<T> {
    constructor(obj: T, settings?: ISerializerSettings, depth?: number);
    protected transform(value: any, propertyKey: string, elementType: ElementType | undefined): any;
}
