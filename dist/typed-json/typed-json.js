"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const typedjson_1 = require("@upe/typedjson");
const lodash_1 = require("lodash");
let ITwJsonObject = class ITwJsonObject {
};
ITwJsonObject = __decorate([
    typedjson_1.JsonObject()
], ITwJsonObject);
exports.ITwJsonObject = ITwJsonObject;
let Any = class Any {
};
Any = __decorate([
    typedjson_1.JsonObject()
], Any);
exports.Any = Any;
let TwJsObject = class TwJsObject {
};
TwJsObject = __decorate([
    typedjson_1.JsonObject()
], TwJsObject);
exports.TwJsObject = TwJsObject;
function getConfigSettings() {
    return typedjson_1.TypedJSON.configSettings;
}
exports.getConfigSettings = getConfigSettings;
function twDeserialize(json, type, settings = {}) {
    settings = Object.assign({}, getConfigSettings(), settings);
    const deserializer = new TwDeserializer(json, type, settings);
    return deserializer.deserialize();
}
exports.twDeserialize = twDeserialize;
function twSerialize(obj, settings = {}) {
    settings = Object.assign({}, getConfigSettings(), settings);
    const serializer = new TwSerializer(obj, settings);
    return serializer.serialize();
}
exports.twSerialize = twSerialize;
class TwDeserializer extends typedjson_1.Deserializer {
    constructor(json, type, settings = {}, depth = 0) { super(json, type, settings, depth); }
    convertDate(value, pk) {
        const ticks = lodash_1.isNumber(value) ? value : Date.parse(value);
        return new Date(ticks);
    }
    processProperty(value, propertyKey, options) {
        return super.processProperty(value, propertyKey, options);
    }
    transform(value, propertyKey, elementType) {
        switch (elementType) {
            case Any: {
                return value;
            }
            case TwJsObject: {
                console.warn(`For prop: ${propertyKey} are used unhandled TwJsObject!!!`);
                return value;
            }
            case Map: {
                return lodash_1.chain(value)
                    .reduce((res, val, key) => {
                    res.set(key, val);
                    return res;
                }, new Map())
                    .value();
            }
            default: {
                return super.transform(value, propertyKey, elementType);
            }
        }
    }
}
exports.TwDeserializer = TwDeserializer;
class TwSerializer extends typedjson_1.Serializer {
    constructor(obj, settings = {}, depth = 0) { super(obj, settings, depth); }
    transform(value, propertyKey, elementType) {
        switch (elementType) {
            case Any: {
                return value;
            }
            case TwJsObject: {
                console.warn(`For prop: ${propertyKey} are used unhandled TwJsObject!!!`);
                return value;
            }
            case Map: {
                const ret = {};
                value.forEach((val, key) => {
                    ret[key] = val;
                });
                return ret;
            }
            default: {
                return super.transform(value, propertyKey, elementType);
            }
        }
    }
}
exports.TwSerializer = TwSerializer;
//# sourceMappingURL=typed-json.js.map