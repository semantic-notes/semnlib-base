import { ConceptClass, IRI, OntoIndividualDto, OntoValue, OwlAnnotations, OwlIRIs, SystemClass, DataIRIs } from './ontology';

export class SemnUtils {

  public static genOntoValue(params: {iri: IRI, value: string}): OntoValue {
    return new OntoValue(params);
  }

  public static genNewConcept(
    params: { label: string },
  ): OntoIndividualDto {
    const value = OntoIndividualDto.create({
      annotations: [
        SemnUtils.genOntoValue({ iri: OwlAnnotations.label, value: params.label }),
      ],
      classes: [
        SemnUtils.genOntoValue({ iri: OwlIRIs.ontoClass, value: ConceptClass.Concept }),
      ],
    });
    return value;
  }

  public static genInfoItem(
    params: { title: string, text: string },
  ): OntoIndividualDto {
    const value = OntoIndividualDto.create({
      classes: [
        SemnUtils.genOntoValue({ iri: OwlIRIs.ontoClass, value: SystemClass.InfoItem }),
      ],
      dataProperties: [
        SemnUtils.genOntoValue({iri: DataIRIs.hasTitle, value: params.title}),
        SemnUtils.genOntoValue({iri: DataIRIs.hasText, value: params.text}),
      ],
    });
    return value;
  }
}
