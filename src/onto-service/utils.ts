import { ElementType, ISerializerSettings, tDeserialize, tSerialize } from '@upe/typedjson';
import { isNil } from 'lodash';
import { none, Option, option } from 'ts-option';
import { isArray } from 'util';

export function genRawToOptionMap<T>(
  type: ElementType<T>,
  alwaysAsArray: boolean = false,
  settings: ISerializerSettings = {}): (raw) => Option<T> {
    return (raw): Option<T> => {
      if (!isNil(raw)) {
        let value: T|T[];
        if (isArray(raw)) {
          value = raw.map(el => tDeserialize<T>(el, type, settings));
        } else {
          const val = tDeserialize<T>(raw, type, settings);
          value = alwaysAsArray ? [val] : val;
        }
        return option<T>(value as any);
      } else {
        return none;
      }
    };
  }

export function genOptionToRaw<T>(
  settings: ISerializerSettings = {}): (raw) => any {
    return (obj: Option<T>): any => {
      return obj.map(value => {
        if (isArray(value)) {
          return value.map(el => tSerialize(el, settings));
        } else {
          return tSerialize(value, settings);
        }
      }).orNull;
    };
  }
