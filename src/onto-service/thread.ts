import { JsonMember, JsonObject } from '@upe/typedjson';
import { merge } from 'lodash';
import { Option } from 'ts-option';

import { IOntology, OntoClassDto, OntoDataPropDto, OntoEntityDto, OntoIndividualDto, OntoRelationDto } from './ontology';
import { genOptionToRaw, genRawToOptionMap } from './utils';

@JsonObject()
export class CreateThread {
  constructor(params: { name: string, description: string }) {
    merge(this, params);
  }

  @JsonMember({type: String}) name: string;
  @JsonMember({type: String}) description: string; 
}

@JsonObject()
export class UserThread extends IOntology {
  @JsonMember({type: String}) uuid: string;
  @JsonMember({type: String}) name: string;
  @JsonMember({type: String}) description: string;
  @JsonMember({
    toJsonMap: genOptionToRaw<OntoEntityDto>(),
    toObjectMap: genRawToOptionMap(OntoEntityDto),
  }) annotations:  Option<OntoEntityDto>;
  @JsonMember({
    toJsonMap: genOptionToRaw<OntoClassDto>(),
    toObjectMap: genRawToOptionMap(OntoClassDto),
  }) classes: Option<OntoClassDto|OntoClassDto[]>;
  @JsonMember({
    toJsonMap: genOptionToRaw<OntoRelationDto>(),
    toObjectMap: genRawToOptionMap(OntoRelationDto),
  }) relations: Option<OntoRelationDto|OntoRelationDto[]>;
  @JsonMember({
    toJsonMap: genOptionToRaw<OntoDataPropDto>(),
    toObjectMap: genRawToOptionMap(OntoDataPropDto),
  }) dataProperties: Option<OntoDataPropDto|OntoDataPropDto[]>;
  @JsonMember({
    toJsonMap: genOptionToRaw<OntoIndividualDto>(),
    toObjectMap: genRawToOptionMap(OntoIndividualDto, true),
  }) individuals: Option<OntoIndividualDto[]>;
}
