import { JsonMember, JsonObject } from '@upe/typedjson';
import { chain, defaults, isEmpty, isNil, merge } from 'lodash';
import { Option } from 'ts-option';
import { genOptionToRaw, genRawToOptionMap } from './utils';

export type IRI = string;

export enum OwlIRIs {
  ontoClass = 'rdf:type',
}

export enum OwlAnnotations {
  label = 'rdfs:label',
  comment = 'rdfs:comment',
}

export enum DataIRIs {
  hasConceptDescr = 'baseOntology:hasItemDescription',
  hasText = 'baseOntology:hasText',
  hasTitle = 'baseOntology:hasTitle',
}

export enum SystemClass {
  InfoItem = 'baseOntology:InfoItem',
  System = 'baseOntology:System',
  Unhandled = 'baseOntology:Unhandled',
}

export enum ConceptClass {
  Concept = 'baseOntology:Concept',
}

export enum SystemRelations {
  mayBeIsRelated = 'baseOntology:mayBeIsRelated',
  wasUpdated = 'baseOntology:wasUpdated',
  wasCreated = 'baseOntology:wasCreated',
}

export const toFilterSystemRelations: string[] = [
  SystemRelations.wasCreated, SystemRelations.wasUpdated,
];

export const systemClasses = [
  SystemClass.InfoItem, SystemClass.System, SystemClass.Unhandled,
];

export const toFilterSystemClasses = [
  SystemClass.System,
];

export function getShortName(iriVal: string): string {
  return chain(iriVal.split(/(#|:)/))
    .last()
    .value()!;
}

@JsonObject()
export class OntoIRI {
  @JsonMember({type: String}) iri: IRI = '';

  constructor(iri?: IRI) {
    if (iri) {
      this.iri = iri;
    }
  }

  public getLocalId(): string {
    return getShortName(this.iri);
  }
}

@JsonObject()
export class OntoValue extends OntoIRI {
  @JsonMember({type: String}) value: string;
  @JsonMember({type: Boolean}) inferred: boolean = false;

  constructor(params?: { iri: IRI, value: string  }) {
    super();
    if (!isNil(params)) {
      merge(this, params);
    }
  }

  public getLocalId(): string {
    return getShortName(this.value);
  }
}

@JsonObject()
export class OntoEntityDto extends OntoIRI {
  @JsonMember({elements: OntoValue}) annotations: OntoValue[] = [];
  constructor(iri?: IRI) { super(iri); }

  public getAnnotationsValues(iri: OwlAnnotations|IRI): OntoValue[] {
    return this.annotations.filter(el => el.iri === iri);
  }
}

@JsonObject()
export class TreeOntoItem extends OntoEntityDto {
  subItems: TreeOntoItem[] = [];

  constructor(iri?: IRI) { super(iri); }
}

@JsonObject()
export class OntoClassDto extends TreeOntoItem {
  @JsonMember({elements: OntoClassDto}) subItems: OntoClassDto[];
}

@JsonObject()
export class OntoRelationDto extends TreeOntoItem {
  @JsonMember({elements: OntoRelationDto}) subItems: OntoRelationDto[];
}

@JsonObject()
export class OntoDataPropDto extends TreeOntoItem {
  @JsonMember({elements: OntoDataPropDto}) subItems: OntoDataPropDto[];
}

@JsonObject()
export class OntoIndividualDto extends OntoEntityDto {
  public static create(params: {
    classes?: OntoValue[];
    annotations?: OntoValue[];
    relations?: OntoValue[];
    dataProperties?: OntoValue[];
    blankNodes?: OntoIndividualDto[];
  } = {}): OntoIndividualDto {
    const ret = new OntoIndividualDto();
    defaults(params, {
      annotations: [],
      blankNodes: [],
      classes: [],
      dataProperties: [],
      relations: [],
    });
    merge(ret, params);
    return ret;
  }

  @JsonMember({elements: OntoValue}) annotations: OntoValue[];
  @JsonMember({elements: OntoValue}) classes: OntoValue[];
  @JsonMember({elements: OntoValue}) relations: OntoValue[];
  @JsonMember({elements: OntoValue}) dataProperties: OntoValue[];
  @JsonMember({elements: OntoIndividualDto}) blankNodes: OntoValue[];

  constructor() { super();}

  public hasClassIri(cls: string): boolean {
    return !isEmpty(this.classes) && this.classes.some(el => el.value === cls);
  }

  public getNameClasses(withSystem = false): string[] {
    return this.classes
      .filter(el => withSystem
        ? true
        : !toFilterSystemClasses.some(cls => cls === el.value))
      .map(el => el.value.split('#')[1]);
  }

  public getRelationValues(iri: IRI): OntoValue[] {
    return this.relations.filter(el => el.iri === iri);
  }

  public getDataValues(iri: DataIRIs|IRI): OntoValue[] {
    return this.dataProperties.filter(el => el.iri === iri);
  }

  public getData(iri: DataIRIs|IRI): string[] {
    return this.getDataValues(iri).map(el => el.value);
  }
}

@JsonObject()
export class IOntology {
  @JsonMember({
    toJsonMap: genOptionToRaw<OntoEntityDto>(),
    toObjectMap: genRawToOptionMap(OntoEntityDto),
  }) annotations:  Option<OntoEntityDto>;
  @JsonMember({
    toJsonMap: genOptionToRaw<OntoClassDto>(),
    toObjectMap: genRawToOptionMap(OntoClassDto),
  }) classes: Option<OntoClassDto|OntoClassDto[]>;
  @JsonMember({
    toJsonMap: genOptionToRaw<OntoRelationDto>(),
    toObjectMap: genRawToOptionMap(OntoRelationDto),
  }) relations: Option<OntoRelationDto|OntoRelationDto[]>;
  @JsonMember({
    toJsonMap: genOptionToRaw<OntoDataPropDto>(),
    toObjectMap: genRawToOptionMap(OntoDataPropDto),
  }) dataProperties: Option<OntoDataPropDto|OntoDataPropDto[]>;
  @JsonMember({
    toJsonMap: genOptionToRaw<OntoIndividualDto>(),
    toObjectMap: genRawToOptionMap(OntoIndividualDto, true),
  }) individuals: Option<OntoIndividualDto[]>;
}
