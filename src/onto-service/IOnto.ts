import { ElementType, ISerializerSettings } from '@upe/typedjson';
import { Option } from 'ts-option';

import { CreateNoteDto } from './notes';
import { OntoIndividualDto, IRI } from './ontology';
import { CreateThread, UserThread } from './thread';
import { User, UserAuth, UserLoginToken } from './user';

export abstract class IOnto {
  public abstract deserialize<T>(json: Object, type: ElementType<T>, settings?: ISerializerSettings): T;
  public abstract serialize<T>(obj: T, settings?: ISerializerSettings): T;
  public abstract loginUserByToken(token: string): Promise<Option<UserAuth>>;
  public abstract loginUserFromTelegram(user: User): Promise<Option<UserAuth>>;
  public abstract loginWithGoogle(
    params: { code: string, scope: string, callBackUrl: string },
  ): Promise<Option<UserAuth>>;
  public abstract refreshToken(auth: UserAuth): Promise<Option<UserAuth>>;
  public abstract genLoginToken(user: User, auth: UserAuth): Promise<Option<UserLoginToken>>;
  public abstract getUserData(auth: UserAuth): Promise<Option<User>>;
  public abstract clearThread(auth: UserAuth, thread: UserThread): Promise<void>;
  public abstract createThread(auth: UserAuth, thread: CreateThread, detailed: boolean): Promise<string>;
  public abstract getThreads(auth: UserAuth): Promise<Option<UserThread[]>>;
  public abstract getThread(auth: UserAuth, uuid: string): Promise<Option<UserThread>>;
  public abstract addNoteToThread(auth: UserAuth, thread: UserThread, note: CreateNoteDto): Promise<void | any>;
  public abstract getNotesInThread(auth: UserAuth, thread: UserThread): Promise<Option<OntoIndividualDto[]>>;
  public abstract getIndividualsInThread(auth: UserAuth, thread: UserThread): Promise<Option<OntoIndividualDto[]>>;
  public abstract getEntity(
    auth: UserAuth,
    thread: UserThread,
    uuid: IRI): Promise<Option<OntoIndividualDto>>;
  public abstract createEntity(
    auth: UserAuth,
    thread: UserThread,
    entity: OntoIndividualDto): Promise<Option<OntoIndividualDto>>;
  public abstract updateEntity(
    auth: UserAuth,
    thread: UserThread,
    entity: OntoIndividualDto,
  ): Promise<Option<OntoIndividualDto>>;
}
