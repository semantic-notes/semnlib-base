import { JsonMember, JsonObject } from '@upe/typedjson';
import { merge } from 'lodash';
import { Option } from 'ts-option';

import { UserThread } from './thread';

@JsonObject()
export class User {
  static fromRaw(params: {
    id: string;
    username: string;
    email?: string;
  }): User {
    return merge(new User(), params);
  }

  @JsonMember({type: String}) id: string;
  @JsonMember({type: String}) username: string;
  @JsonMember({type: String}) email: string;
}

@JsonObject()
export class UserAuth {
  @JsonMember({type: String}) id: string;
  @JsonMember({type: Number}) tokenExpTime: number;
  @JsonMember({type: String}) token: string;
  @JsonMember({type: String}) refresh: string;
}

@JsonObject()
export class UserLoginToken {
  @JsonMember({type: String}) token: string;
}

export interface UserData {
  token: UserAuth;
  user: User;
  selectedThread: Option<UserThread>;
}
