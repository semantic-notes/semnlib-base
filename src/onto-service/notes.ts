import { merge } from "lodash";
import { JsonObject, JsonMember } from "@upe/typedjson";

@JsonObject()
export class CreateNoteDto {
  constructor(params: { text: string, title?: string }) {
    merge(this, params);
  }

  @JsonMember({type: String}) title: string;
  @JsonMember({type: String}) text: string;
}
