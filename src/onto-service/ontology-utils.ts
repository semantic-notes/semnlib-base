import { TreeOntoItem } from './ontology';

export type OntoEntityPath = TreeOntoItem[];

export function getEntityPath(root: TreeOntoItem, iri: string): TreeOntoItem[] {
  const paths: TreeOntoItem[] = [];
  const doEntityStep = (entity: TreeOntoItem): boolean => {
    paths.push(entity);
    if (entity.iri === iri) {
      return true;
    } else {
      if (entity.subItems.some(doEntityStep)) {
        return true;
      } else {
        paths.pop();
        return false;
      }
    }
  };

  doEntityStep(root);
  return paths;
}

export function compareOntoPath(lha: OntoEntityPath, rha: OntoEntityPath): number {
  const lhaHash: string = lha.map(el => el.getLocalId()).join('');
  const rhaHash: string = rha.map(el => el.getLocalId()).join('');
  return  ( ( lhaHash === rhaHash ) ? 0 : ( ( lhaHash > rhaHash ) ? 1 : -1 ) );
}
