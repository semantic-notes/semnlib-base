import { ISerializerSettings, tDeserialize, tSerialize } from '@upe/typedjson';
import { ElementType } from '@upe/typedjson/dist/utils';
import axios, { AxiosInstance, AxiosRequestConfig, AxiosResponse, AxiosError } from 'axios';
import { isEmpty } from 'lodash';
import { none, Option, option, some } from 'ts-option';
import { URL } from 'url';

import { IOnto } from './IOnto';
import { CreateNoteDto } from './notes';
import { OntoIndividualDto, IRI } from './ontology';
import { CreateThread, UserThread as UserThread } from './thread';
import { User, UserAuth, UserLoginToken } from './user';
import { Subject } from 'rxjs';

let isNode = false;
let Agent;
let otplib;

try {
  isNode = Object.prototype.toString.call(global.process) === '[object process]';
  if (isNode) {
    // tslint:disable-next-line:no-var-requires
    const https = require('https');
    Agent = https.Agent;
    // tslint:disable-next-line:no-var-requires
    otplib = require('otplib');
  }
} catch(e) {}

export class OntoService implements IOnto {

  private net: AxiosInstance;

  private url: URL;

  // tslint:disable-next-line:member-access
  // tslint:disable-next-line:member-ordering
  public errors: Subject<AxiosError> = new Subject();

  constructor(
    private baseUri = 'http://localhost:9000/api',
    private base32SecretBase: string = '',
  ) {
    if (isNode) {
      const step = 30;
      otplib.authenticator.options = {
        step: step,
        window: 3,
      };
      this.url = new URL(this.baseUri);
      if (this.url.protocol === 'https:') {
        this.net = axios.create({
          httpsAgent: new Agent({
            rejectUnauthorized: false,
          }),
        });
      } else {
        this.net = axios.create();
      }
    } else {
      this.net = axios.create();
    }
  }

  public handleError(error) {
    this.errors.next(error);
  }

  public deserialize<T>(json: Object,
                        type: ElementType<T>,
                        settings: ISerializerSettings = {}): T {
    return tDeserialize(json, type, settings);
  }

  public serialize<T>(obj: T, settings: ISerializerSettings = {}): T {
    return tSerialize(obj, settings);
  }

  public loginUserByToken(token: string): Promise<Option<UserAuth>> {
    const url = `${this.baseUri}/login-by-token`;
    return this.net.post<UserAuth>(
      url,
      {},
      { headers: { Authorization: `Bearer ${token}`  } })
      .then(
        response => option(this.deserialize(response.data, UserAuth)),
        error => {
          this.handleError(error);
          throw error;
        },
      );
  }

  public loginWithGoogle(
    params: { code: string, scope: string, callBackUrl: string },
  ): Promise<Option<UserAuth>> {
    const url = `${this.baseUri}/login-with-google?code=${params.code}&scope=${params.scope}`;
    return this.net.get(url, { headers: { 'callback-url': params.callBackUrl } })
      .then(
        response => option(this.deserialize(response.data, UserAuth)),
        error => {
          this.handleError(error);
          throw error;
        },
      );
  }

  public loginUserFromTelegram(user: User): Promise<Option<UserAuth>> {
    const token = otplib.authenticator.generate(this.base32SecretBase);
    const userDto = this.serialize(user);
    const url = `${this.baseUri}/login-for-telegram`;
    return this.net.post<UserAuth>(url, userDto, { headers: { totp: token  } })
      .then(
        response => option(this.deserialize(response.data, UserAuth)),
        error => {
          this.handleError(error);
          throw error;
        },
      );
  }

  public refreshToken(auth: UserAuth): Promise<Option<UserAuth>> {
    const url = `${this.baseUri}/refresh`;
    return this.net.post<UserAuth>(url, {}, {
      headers: { Authorization: auth.refresh }
    })
      .then(response => option(this.deserialize(response.data, UserAuth)))
      .catch(error => {
        this.handleError(error);
        return none;
      });
  }

  public genLoginToken(user: User, auth: UserAuth): Promise<Option<UserLoginToken>> {
    const from = 'telegram';
    const token = otplib.authenticator.generate(this.base32SecretBase);
    const url = `${this.baseUri}/get-login-token/${auth.id}?from=${from}`;
    const userDto = this.serialize(user);
    return this.net.post<UserLoginToken>(
      url,
      userDto,
      {
        headers: {
          Authorization: auth.token,
          totp: token,
        },
      },
    ).then(response => option(this.deserialize(response.data, UserLoginToken)))
     .catch(error => {
      this.handleError(error);
       return none;
     });
  }

  public getUserData(auth: UserAuth): Promise<Option<User>> {
    const url = `${this.baseUri}/users/${auth.id}/data`;
    return this.net.get<User>(url, this.getNetParams(auth))
      .then(resp => option(this.deserialize(resp.data, User)))
      .catch((error) => {
        this.handleError(error);
        return none;
      });
  }

  public getThread(auth: UserAuth, uuid: string, detailed: boolean = false): Promise<Option<UserThread>> {
    const url = `${this.baseUri}/users/${auth.id}/threads/${uuid}?detailed=${detailed}`;
    return this.net.get<UserThread>(url, this.getNetParams(auth))
      .then(resp => option(this.deserialize(resp.data, UserThread)))
      .catch((error) => {
        this.handleError(error);
        return none;
      });
  }

  public createThread(auth: UserAuth, thread: CreateThread): Promise<string> {
    const url = `${this.baseUri}/users/${auth.id}/threads`;
    const dto = this.serialize(thread);
    return this.net.post<void>(url, dto, this.getNetParams(auth))
      .then((res) => `${res.data}`);
  }

  public getThreads(auth: UserAuth): Promise<Option<UserThread[]>> {
    const url = `${this.baseUri}/users/${auth.id}/threads`;
    return this.net.get<UserThread[]>(url, this.getNetParams(auth)).then(resp => {
      const items: any[] = resp.data;
      const dtos = items.map(el => this.deserialize(el, UserThread));
      return isEmpty(dtos) ? none : some(dtos);
    })
    .catch(error => {
      this.handleError(error);
      return none;
    });
  }

  public addNoteToThread(auth: UserAuth, thread: UserThread, note: CreateNoteDto): Promise<void | any> {
    const url = `${this.baseUri}/users/${auth.id}/threads/${thread.uuid}/notes`;
    const dto = this.serialize(note);
    return this.net.post<void>(url, dto, this.getNetParams(auth))
      .then(res => {
        return Promise.resolve(res);
      })
      .catch(error => {
        this.handleError(error);
      });
  }

  public getIndividualsInThread(auth: UserAuth, thread: UserThread): Promise<Option<OntoIndividualDto[]>> {
    const url = `${this.baseUri}/users/${auth.id}/threads/${thread.uuid}/ind`;
    return this.net.get<OntoIndividualDto[]>(url, this.getNetParams(auth)).then(res => {
      const items: any[] = res.data;
      const dtos = items.map(el => this.deserialize(el, OntoIndividualDto));
      return isEmpty(dtos) ? none : some(dtos);
    }).catch(error => {
      this.handleError(error);
      return none;
    });
  }

  public getNotesInThread(auth: UserAuth, thread: UserThread): Promise<Option<OntoIndividualDto[]>> {
    const url = `${this.baseUri}/users/${auth.id}/threads/${thread.uuid}/notes`;
    return this.net.get<OntoIndividualDto[]>(url, this.getNetParams(auth)).then(res => {
      const items: any[] = res.data;
      const dtos = items.map(el => this.deserialize(el, OntoIndividualDto));
      return isEmpty(dtos) ? none : some(dtos);
    }).catch(error => {
      this.handleError(error);
      return none;
    });
  }

  public getEntity(
    auth: UserAuth,
    thread: UserThread,
    uuid: IRI): Promise<Option<OntoIndividualDto>> {
      const url = `${this.baseUri}/users/${auth.id}/threads/${thread.uuid}/ind/${uuid}`;
      return this.net.get<OntoIndividualDto>(
        url,
        this.getNetParams(auth),
      ).then(res => {
        return some(this.deserialize(res.data, OntoIndividualDto));
      }).catch(error => {
        this.handleError(error);
        return none;
      });
  }

  public createEntity(
    auth: UserAuth,
    thread: UserThread,
    entity: OntoIndividualDto): Promise<Option<OntoIndividualDto>> {
      const url = `${this.baseUri}/users/${auth.id}/threads/${thread.uuid}/ind`;
      return this.net.post<OntoIndividualDto>(
        url,
        this.serialize(entity),
        this.getNetParams(auth),
      ).then(res => {
        return some(this.deserialize(res.data, OntoIndividualDto));
      }).catch(error => {
        this.handleError(error);
        return none;
      });
    }

  public updateEntity(
    auth: UserAuth,
    thread: UserThread,
    entity: OntoIndividualDto): Promise<Option<OntoIndividualDto>> {
      const url = `${this.baseUri}/users/${auth.id}/threads/${thread.uuid}/ind/${entity.getLocalId()}`;
      return this.net.put<OntoIndividualDto>(
        url,
        this.serialize(entity),
        this.getNetParams(auth),
      ).then(res => {
        return some(this.deserialize(res.data, OntoIndividualDto));
      }).catch(error => {
        this.handleError(error);
        return none;
      });
  }


  public clearThread(auth: UserAuth, thread: UserThread): Promise<void> {
    const url = `${this.baseUri}/users/${auth.id}/threads/${thread.uuid}/clear`;
    return this.net.put<void>(url, {}, this.getNetParams(auth))
      .then(res => Promise.resolve());
  }

  private getNetParams(data: UserAuth): AxiosRequestConfig {
    return {
      headers: {
        Authorization: data.token,
      },
    };
  }

}
